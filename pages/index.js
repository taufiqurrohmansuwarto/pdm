import { Alert, Layout } from "antd";
import DataSatker from "../src/DataSatker";

const { Header, Footer, Sider, Content } = Layout;

const Index = () => {
  return (
    <Layout>
      <Header>Test</Header>
      <Content>
        <div style={{ padding: 10 }}>
          <Alert
            message="Perhatian"
            description="Data di rekap pada tanggal 30 MARET 2022 jam 23.00. Pensiun 2021 dan 2022 tidak mengikuti PDM (Pemutakhiran Data Mandiri)"
            type="warning"
            showIcon
            closable
          />
          <DataSatker />
        </div>
      </Content>
      <Footer style={{ textAlign: "center" }}>
        versi : 0.0.0c Sub Bidang Pengolahan Data dan Sistem Informasi ©2021
      </Footer>
    </Layout>
  );
};

export default Index;
