import nc from "next-connect";
const handler = nc();
const sumBy = require("lodash/sumBy");

const dataSatker = require("../../../data/satker.json");

const totalBulshit = (data) => {
  return data?.map((d) => {
    const {
      total,
      status_aktivasi: totalAktivasi,
      data_pribadi,
      golongan,
      pendidikan,
      jabatan,
      pmk,
      cpns_pns,
      diklat,
      keluarga,
      skp,
      penghargaan,
      organisasi,
      cltn,
    } = d;
    const persentaseFull = total * 12;
    const currentTotal =
      parseInt(data_pribadi, 10) +
      parseInt(golongan, 10) +
      parseInt(pmk, 10) +
      parseInt(cpns_pns, 10) +
      parseInt(diklat, 10) +
      parseInt(keluarga, 10) +
      parseInt(skp, 10) +
      parseInt(penghargaan, 10) +
      parseInt(organisasi, 10) +
      parseInt(cltn, 10) +
      parseInt(pendidikan, 10) +
      parseInt(jabatan, 10);

    const currentPresentase = (currentTotal / persentaseFull) * 100;
    const presentase = Number(currentPresentase?.toFixed(2));

    return {
      ...d,
      currentPresentase: presentase,
    };
  });
};

export default handler.get((req, res) => {
  const rerata =
    sumBy(totalBulshit(dataSatker), "currentPresentase") / dataSatker?.length;
  res.json({
    data: totalBulshit(dataSatker),
    rerata: Number(rerata?.toFixed(2)),
  });
});
