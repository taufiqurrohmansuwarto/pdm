import nc from "next-connect";
const handler = nc();
const data = require("../../../../data/simple.json");

export default handler.get((req, res) => {
  res.json(data);
});
