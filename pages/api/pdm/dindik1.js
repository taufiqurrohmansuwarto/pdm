import { chunk } from "lodash";
import nc from "next-connect";
const handler = nc();

const dataEmployee = require("../../../data/employee-satker.json");

export const config = {
  api: {
    bodyParser: {
      sizeLimit: "20mb",
    },
  },
};

export default handler.get((req, res) => {
  const hasilku = dataEmployee?.["DINAS PENDIDIKAN"];
  const dibagi = hasilku?.length / 3;
  const [data] = chunk(hasilku, dibagi);

  const result = data?.map((d) => {
    const { skpd, konfirmasi_udm, ...lastResult } = d;
    // ambil yang dokumen saja
    const { nama, nip, satker, status_aktivasi, ...dokumenUdm } = lastResult;

    const totalDokumenKosong = Object.values(dokumenUdm);
    let total = 0;
    totalDokumenKosong.forEach((x) => {
      if (x === 1) {
        total += 1;
      }
    });

    return {
      ...lastResult,
      total_dokumen_selesai: parseInt(total, 10),
      total_dokumen_kurang: 12 - parseInt(total, 10),
    };
  });

  const hasil = result?.filter((d) => d?.total_dokumen_selesai < 12);
  res.json({ data: hasil });
});
