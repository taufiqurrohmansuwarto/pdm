import { Button, Progress, Space, Table, Tabs, Typography } from "antd";
import axios from "axios";
import * as FileSaver from "file-saver";
import moment from "moment";
import React, { useState } from "react";
// import { useJsonToCsv } from "react-json-csv";
import { useQuery } from "react-query";
import xlsx from "xlsx";

const fileType =
  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
const fileExtension = ".xlsx";

const DataSatker = () => {
  const [loading, setLoading] = useState(false);
  const { data, isLoading } = useQuery("pdm", async () => {
    return axios.get("/api/pdm").then((res) => res?.data);
  });

  const downloadFull = async () => {
    const { data: result } = await axios.get("/api/pdm");
    const hasilku = xlsx.utils.json_to_sheet(result?.data, {
      header: ["instansi", "currentPresentase"],
    });
    const xlbuffer = xlsx.write(
      {
        Sheets: {
          ["hasil"]: hasilku,
        },
        SheetNames: ["hasil"],
      },
      {
        bookType: "xlsx",
        type: "array",
      }
    );

    const hasil = new Blob([xlbuffer], { type: fileType });

    FileSaver.saveAs(hasil, "Hasil" + fileExtension);
  };

  const { data: dataCabangDinas, isLoading: isLoadingCabangDinas } = useQuery(
    "pdm-cabang-dinas",
    async () => {
      return axios.get("/api/pdm/cabang-dinas").then((res) => res?.data);
    }
  );

  const { data: dataSimple, isLoading: isLoadingSimple } = useQuery(
    "pdm-simple",
    async () => {
      return axios.get("/api/pdm/simple").then((res) => res?.data);
    }
  );

  const downlodaData = async () => {
    const { data: result } = await axios.get("/api/pdm/simple");
    const hasilku = xlsx.utils.json_to_sheet(result, {
      // header: ["nama", "nip"],
    });
    const xlbuffer = xlsx.write(
      {
        Sheets: {
          ["hasil"]: hasilku,
        },
        SheetNames: ["hasil"],
      },
      {
        bookType: "xlsx",
        type: "array",
      }
    );

    const hasil = new Blob([xlbuffer], { type: fileType });

    FileSaver.saveAs(hasil, "download" + fileExtension);
  };

  const getCsvCabangDinas = async (cabdin) => {
    const { data: result } = await axios.get(`/api/pdm/cabang-dinas/${cabdin}`);
    const hasilku = xlsx.utils.json_to_sheet(result?.data, {
      header: ["nama", "nip"],
    });
    const xlbuffer = xlsx.write(
      {
        Sheets: {
          ["hasil"]: hasilku,
        },
        SheetNames: ["hasil"],
      },
      {
        bookType: "xlsx",
        type: "array",
      }
    );

    const hasil = new Blob([xlbuffer], { type: fileType });

    FileSaver.saveAs(hasil, cabdin + fileExtension);
  };

  const getCsv = async (satker) => {
    if (satker === "DINAS PENDIDIKAN") {
      const { data: result1 } = await axios.get(`/api/pdm/dindik1`);
      const { data: result2 } = await axios.get(`/api/pdm/dindik2`);
      const { data: result3 } = await axios.get(`/api/pdm/dindik3`);

      const hasilgw = [...result1?.data, ...result2?.data, ...result3?.data];

      const hasilku = xlsx.utils.json_to_sheet(hasilgw);

      const xlbuffer = xlsx.write(
        {
          Sheets: {
            ["hasil"]: hasilku,
          },
          SheetNames: ["hasil"],
        },
        {
          bookType: "xlsx",
          type: "array",
        }
      );

      const hasil = new Blob([xlbuffer], { type: fileType });

      FileSaver.saveAs(hasil, satker + fileExtension);
    } else {
      const { data: result } = await axios.get(`/api/pdm/${satker}`);
      const hasilku = xlsx.utils.json_to_sheet(result?.data, {
        header: ["nama", "nip"],
      });
      const xlbuffer = xlsx.write(
        {
          Sheets: {
            ["hasil"]: hasilku,
          },
          SheetNames: ["hasil"],
        },
        {
          bookType: "xlsx",
          type: "array",
        }
      );

      const hasil = new Blob([xlbuffer], { type: fileType });

      FileSaver.saveAs(hasil, satker + fileExtension);
    }
  };

  const boldNumber = (name) => (text) => {
    const test = parseInt(text[name], 10) - parseInt(text["total"]);
    const positif = test < 0 ? test * -1 : test;

    return (
      <>
        <Typography.Text strong type={positif === 0 ? "success" : "danger"}>
          {parseInt(text[name], 10)}
        </Typography.Text>
        {positif !== 0 && (
          <>
            {" "}
            (
            <Typography.Text type="danger" strong>
              -{positif}
            </Typography.Text>
            )
          </>
        )}
      </>
    );
  };

  const columnCabangDinas = [
    {
      title: "No",
      key: "no",
      width: 50,
      render: (a, b, c) => <Typography.Text>{c + 1}</Typography.Text>,
      fixed: "left",
    },
    {
      title: "Cabang Dinas Pendidikan",
      key: "cabang_dinas",
      width: 320,
      fixed: "left",
      dataIndex: "cabang_dinas",
    },
    {
      title: "Progress",
      key: "progress",
      width: 300,
      render: (row) => (
        <Progress
          percent={row?.currentPresentase}
          size="small"
          showInfo={true}
        />
      ),
      sorter: (a, b) => a?.currentPresentase - b?.currentPresentase,
    },
    {
      title: "Pegawai",
      key: "total",
      dataIndex: "total",
    },
    {
      title: "Aktivasi",
      key: "status_aktivasi",
      render: boldNumber("status_aktivasi"),
    },
    {
      title: "Profil",
      render: boldNumber("data_pribadi"),
      key: "data_pribadi",
    },
    { title: "Golongan", render: boldNumber("golongan"), key: "golongan" },
    {
      title: "Pendidikan",
      render: boldNumber("pendidikan"),
      key: "pendidikan",
    },
    { title: "Jabatan", render: boldNumber("jabatan"), key: "jabatan" },
    { title: "PMK", render: boldNumber("pmk"), key: "pmk" },
    { title: "CPNS/PNS", render: boldNumber("cpns_pns"), key: "cpns_pns" },
    { title: "Diklat", render: boldNumber("diklat"), key: "diklat" },
    { title: "Keluarga", render: boldNumber("keluarga"), key: "keluarga" },
    { title: "SKP", render: boldNumber("skp"), key: "skp" },
    {
      title: "Penghargaan",
      render: boldNumber("penghargaan"),
      key: "penghargaan",
    },
    {
      title: "Organisasi",
      render: boldNumber("organisasi"),
      key: "organisasi",
    },
    { title: "CLTN", render: boldNumber("cltn"), key: "cltn" },
    {
      title: "Download",
      fixed: "right",
      key: "download",
      render: (row) => (
        <Button
          type="primary"
          onClick={async () => await getCsvCabangDinas(row?.cabang_dinas)}
        >
          Download
        </Button>
      ),
    },
  ];

  const columnDataSimple = [
    {
      title: "No",
      key: "no",
      width: 10,
      render: (a, b, c) => <Typography.Text>{c + 1}</Typography.Text>,
    },
    {
      title: "Perangkat Daerah",
      key: "instansi",
      width: 50,
      dataIndex: "instansi",
    },
    {
      title: "Jumlah Pegawai",
      key: "total",

      width: 20,
      dataIndex: "total",
    },
    {
      title: "Belum Selesai",
      key: "belum_selesai",

      width: 20,
      dataIndex: "belum_selesai",

      sorter: (a, b) => a?.belum_selesai - b?.belum_selesai,
    },
    {
      title: "Download",
      key: "download",
      width: 20,
      render: (row) => (
        <Button
          type="primary"
          onClick={async () => await getCsv(row?.instansi)}
        >
          Download
        </Button>
      ),
    },
  ];

  const columns = [
    {
      title: "No",
      key: "no",
      width: 50,
      render: (a, b, c) => <Typography.Text>{c + 1}</Typography.Text>,
      fixed: "left",
    },
    {
      title: "Instansi",
      key: "instansi",
      width: 320,
      fixed: "left",
      dataIndex: "instansi",
    },
    {
      title: "Progress",
      key: "progress",
      width: 300,
      render: (row) => (
        <Progress
          percent={row?.currentPresentase}
          size="small"
          showInfo={true}
        />
      ),
      sorter: (a, b) => a?.currentPresentase - b?.currentPresentase,
    },
    {
      title: "Pegawai",
      key: "total",
      dataIndex: "total",
    },
    {
      title: "Aktivasi",
      key: "status_aktivasi",
      render: boldNumber("status_aktivasi"),
    },
    {
      title: "Profil",
      render: boldNumber("data_pribadi"),
      key: "data_pribadi",
    },
    { title: "Golongan", render: boldNumber("golongan"), key: "golongan" },
    {
      title: "Pendidikan",
      render: boldNumber("pendidikan"),
      key: "pendidikan",
    },
    { title: "Jabatan", render: boldNumber("jabatan"), key: "jabatan" },
    { title: "PMK", render: boldNumber("pmk"), key: "pmk" },
    { title: "CPNS/PNS", render: boldNumber("cpns_pns"), key: "cpns_pns" },
    { title: "Diklat", render: boldNumber("diklat"), key: "diklat" },
    { title: "Keluarga", render: boldNumber("keluarga"), key: "keluarga" },
    { title: "SKP", render: boldNumber("skp"), key: "skp" },
    {
      title: "Penghargaan",
      render: boldNumber("penghargaan"),
      key: "penghargaan",
    },
    {
      title: "Organisasi",
      render: boldNumber("organisasi"),
      key: "organisasi",
    },
    { title: "CLTN", render: boldNumber("cltn"), key: "cltn" },
    {
      title: "Download",
      fixed: "right",
      key: "download",
      render: (row) => (
        <Button
          type="primary"
          onClick={async () => await getCsv(row?.instansi)}
        >
          Download
        </Button>
      ),
    },
  ];

  const dueDate = moment("2021-10-31");

  return (
    <div style={{ paddingLeft: 20, paddingRight: 20, marginTop: 10 }}>
      <Space direction="vertical">
        {/* {data && <Progress type="circle" percent={data?.rerata} />} */}
      </Space>
      <Tabs>
        <Tabs.TabPane tab="Sederhana" key="0">
          {dataSimple && (
            <div>
              <Button onClick={async () => await downlodaData()}>
                Download
              </Button>
              <Table
                bordered
                loading={isLoadingSimple}
                rowKey={(row) => row?.instansi}
                columns={columnDataSimple}
                dataSource={dataSimple}
                pagination={false}
                scroll={{ y: 400 }}
              />
            </div>
          )}
        </Tabs.TabPane>

        <Tabs.TabPane tab="Perangkat Daerah" key="1">
          <Button onClick={async () => await downloadFull()}>
            Download Full
          </Button>
          <Table
            bordered
            loading={isLoading}
            rowKey={(row) => row?.instansi}
            columns={columns}
            dataSource={data?.data}
            pagination={false}
            scroll={{ y: 400, x: 2000 }}
            size="small"
          />
        </Tabs.TabPane>
        <Tabs.TabPane tab="Cabang Dinas Pendidikan" key="2">
          <Table
            bordered
            loading={isLoading}
            rowKey={(row) => row?.cabang_dinas}
            columns={columnCabangDinas}
            dataSource={dataCabangDinas?.data}
            pagination={false}
            scroll={{ y: 400, x: 2000 }}
            size="small"
          />
        </Tabs.TabPane>
      </Tabs>
    </div>
  );
};

export default DataSatker;
